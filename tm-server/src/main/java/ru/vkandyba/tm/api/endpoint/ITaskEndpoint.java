package ru.vkandyba.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.TaskDTO;
import ru.vkandyba.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    );

    @WebMethod
    void addTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "task", partName = "task") @NotNull TaskDTO task
    );

    @WebMethod
    void finishTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @WebMethod
    void finishTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @WebMethod
    void removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    TaskDTO showTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );


    @WebMethod
    void startTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @WebMethod
    void startTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @WebMethod
    void updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description

    );

    @WebMethod
    void removeTaskWithTasksById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @WebMethod
    void clearTasks(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    );

    @WebMethod
    void unbindTaskFromProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @WebMethod
    List<TaskDTO> showAllTasksByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    );

    @WebMethod
    void bindTaskToProjectById(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );
}
