package ru.vkandyba.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.dto.SessionDTO;
import ru.vkandyba.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO viewUserInfo(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    );

    @WebMethod
    void changeUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

    @WebMethod
    void updateUserProfile(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "midName", partName = "midName") @NotNull String midName
    );
}
