package ru.vkandyba.tm.repository.dto;

import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.TaskDTO;
import ru.vkandyba.tm.enumerated.Status;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskDtoRepository extends AbstractDtoRepository<TaskDTO> {

    public TaskDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public List<TaskDTO> findAll(@NotNull String userId){
        List<TaskDTO> list = entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        if(list.isEmpty())
            return null;
        return list;
    }

    public TaskDTO findById(@NotNull String userId, @NotNull String id){
        List<TaskDTO> list = entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public TaskDTO findByName(@NotNull String userId, @NotNull String name){
        List<TaskDTO> list = entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public void removeByName(@NotNull String userId, @NotNull String name){
        TaskDTO taskDTO = findByName(userId, name);
        remove(taskDTO);
    }

    public void startById(@NotNull String userId, @NotNull String id){
        TaskDTO taskDTO = findById(userId, id);
        taskDTO.setStatus(Status.IN_PROGRESS);
        update(taskDTO);
    }

    public void startByName(@NotNull String userId, @NotNull String name){
        TaskDTO taskDTO = findByName(userId, name);
        taskDTO.setStatus(Status.IN_PROGRESS);
        update(taskDTO);
    }

    public void finishById(@NotNull String userId, @NotNull String id){
        TaskDTO taskDTO = findById(userId, id);
        taskDTO.setStatus(Status.COMPLETED);
        update(taskDTO);
    }

    public void finishByName(@NotNull String userId, @NotNull String name){
        TaskDTO taskDTO = findByName(userId, name);
        taskDTO.setStatus(Status.COMPLETED);
        update(taskDTO);
    }

    public void updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description){
        TaskDTO taskDTO = findById(userId, id);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        update(taskDTO);
    }

    public void bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId){
        TaskDTO taskDTO = findById(userId, taskId);
        taskDTO.setProjectId(projectId);
        update(taskDTO);
    }

    public void unbindTaskToProjectById(@NotNull String userId, @NotNull String taskId){
        TaskDTO taskDTO = findById(userId, taskId);
        taskDTO.setProjectId(null);
        update(taskDTO);
    }

    public List<TaskDTO> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId){
        List<TaskDTO> list = entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
        if(list.isEmpty())
            return null;
        return list;
    }

    public void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId){
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId AND e.projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    public void clear(@NotNull String userId){
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
