package ru.vkandyba.tm.exception.system;

import ru.vkandyba.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect");
    }
}
