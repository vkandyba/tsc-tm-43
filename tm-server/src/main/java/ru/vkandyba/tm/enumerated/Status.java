package ru.vkandyba.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Status {

    NON_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Complete");

    @NotNull
    final private String displayName;

    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

    public String getName(){
        return displayName;
    }

}
