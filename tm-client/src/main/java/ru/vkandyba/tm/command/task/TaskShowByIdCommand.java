package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.SessionDTO;
import ru.vkandyba.tm.endpoint.Task;
import ru.vkandyba.tm.endpoint.TaskDTO;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.List;
import java.util.Optional;

public class TaskShowByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by id...";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSession();
        System.out.println("Enter Id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final TaskDTO task = serviceLocator.getTaskEndpoint().showTaskById(session, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        @NotNull final List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().findAllTasks(session);
        System.out.println("Index: " + tasks.indexOf(task));
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
