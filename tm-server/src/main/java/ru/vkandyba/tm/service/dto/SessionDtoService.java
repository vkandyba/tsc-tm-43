package ru.vkandyba.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.dto.SessionDTO;
import ru.vkandyba.tm.dto.UserDTO;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.repository.dto.SessionDtoRepository;
import ru.vkandyba.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class SessionDtoService extends AbstractDtoService{

    @NotNull
    private final UserDtoService userDtoService;

    public SessionDtoService(@NotNull IConnectionService connectionService, @NotNull UserDtoService userDtoService) {
        super(connectionService);
        this.userDtoService = userDtoService;
    }

    public void close(@Nullable final SessionDTO sessionDTO) {
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
            repository.remove(sessionDTO);
            entityManager.getTransaction().commit();
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public SessionDTO open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final UserDTO user = userDtoService.findByLogin(login);
        if (user == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setTimestamp(System.currentTimeMillis());
        final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            final SessionDtoRepository repository = new SessionDtoRepository(entityManager);
            repository.add(sessionDTO);
            entityManager.getTransaction().commit();
            return sign(sessionDTO);
        } catch (Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login.isEmpty() || password.isEmpty()) return false;
        final UserDTO user = userDtoService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(connectionService.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @SneakyThrows
    public void validate(SessionDTO sessionDTO) {
        if(sessionDTO == null) throw new AccessDeniedException();
        if(sessionDTO.getSignature() == null || sessionDTO.getSignature().isEmpty()) throw new AccessDeniedException();
        if(sessionDTO.getUserId() == null || sessionDTO.getUserId().isEmpty()) throw new AccessDeniedException();
        if(sessionDTO.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final SessionDTO temp = sessionDTO.clone();
        @NotNull final String signatureSource = sessionDTO.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if(!check) throw new AccessDeniedException();
    }

    public void validate(SessionDTO sessionDTO, Role role) {
        validate(sessionDTO);
        @Nullable final UserDTO user = userDtoService.findById(sessionDTO.getUserId());
        if(user == null) throw new AccessDeniedException();
        if(!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    public @Nullable SessionDTO sign(SessionDTO sessionDTO) {
        if(sessionDTO == null) return null;
        sessionDTO.setSignature(null);
        @Nullable final String signature = HashUtil.sign(connectionService.getPropertyService(), sessionDTO);
        sessionDTO.setSignature(signature);
        return sessionDTO;
    }

}
