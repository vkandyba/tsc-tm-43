package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.dto.UserDTO;

public interface IUserService{

    UserDTO findByLogin(String login);

    UserDTO findById(String id);

    void removeUser(UserDTO user);

    void removeByLogin(String login);

    void create(String login, String password, String email);

    void create(String login, String password, Role role);

    void create(String login, String password);

    void updateUser(String userId, String firstName, String lastName, String middleName);

    boolean isLoginExists(String login);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
