package ru.vkandyba.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public List<Project> findAll(@NotNull String userId){
        List<Project> list = entityManager
                .createQuery("SELECT e FROM Project e WHERE e.userId = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
        if(list.isEmpty())
            return null;
        return list;
    }

    public Project findById(@NotNull String userId, @NotNull String id){
        List<Project> list = entityManager
                .createQuery("SELECT e FROM Project e WHERE e.userId = :userId AND e.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public Project findByName(@NotNull String userId, @NotNull String name){
        List<Project> list = entityManager
                .createQuery("SELECT e FROM Project e WHERE e.userId = :userId AND e.name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1).getResultList();
        if(list.isEmpty())
            return null;
        return list.get(0);
    }

    public void removeByName(@NotNull String userId, @NotNull String name){
        Project project = findByName(userId, name);
        remove(project);
    }

    public void startById(@NotNull String userId, @NotNull String id){
        Project project = findById(userId, id);
        project.setStatus(Status.IN_PROGRESS);
        update(project);
    }

    public void startByName(@NotNull String userId, @NotNull String name){
        Project project = findByName(userId, name);
        project.setStatus(Status.IN_PROGRESS);
        update(project);
    }

    public void finishById(@NotNull String userId, @NotNull String id){
        Project project = findById(userId, id);
        project.setStatus(Status.COMPLETED);
        update(project);
    }

    public void finishByName(@NotNull String userId, @NotNull String name){
        Project project = findByName(userId, name);
        project.setStatus(Status.COMPLETED);
        update(project);
    }

    public void updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description){
        Project project = findById(userId, id);
        project.setName(name);
        project.setDescription(description);
        update(project);
    }

    public void clear(@NotNull String userId){
        entityManager.createQuery("DELETE FROM Project e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
